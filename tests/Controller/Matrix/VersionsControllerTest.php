<?php

namespace App\Tests\Controller\Matrix;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VersionsControllerTest extends WebTestCase
{
    public function testDefault(): void
    {
        $client = static::createClient();

        $client->xmlHttpRequest('GET', '/_matrix/client/versions');
        $content = json_decode($client->getResponse()->getContent(), true);

        $expected = [
            'versions' => [
                'v1.8',
            ],
        ];

        $this->assertEquals($expected, $content);
    }

    /**
     * This is a smoke test for CORS.
     *
     * We don’t need to test it on every route.
     */
    public function testCORS(): void
    {
        $client = static::createClient();

        $client->xmlHttpRequest('GET', '/_matrix/client/versions');

        $headers = $client->getResponse()->headers;

        $expected = [
            '*',
            'GET, POST, PUT, DELETE, OPTIONS',
            'X-Requested-With, Content-Type, Authorization',
        ];

        $this->assertEquals($expected, [
            $headers->get('Access-Control-Allow-Origin'),
            $headers->get('Access-Control-Allow-Methods'),
            $headers->get('Access-Control-Allow-Headers'),
        ]);

        $expected = [
            'versions' => [
                'v1.8',
            ],
        ];

        $content = json_decode($client->getResponse()->getContent(), true);

        // With a GET request on `/_matrix/client/versions`, we should get
        // CORS headers **and** a response content
        $this->assertEquals($expected, $content);
    }

    /**
     * This is a smoke test for OPTIONS requests.
     *
     * We don’t need to test it on every route.
     */
    public function testOptions(): void
    {
        $client = static::createClient();

        $client->xmlHttpRequest('OPTIONS', '/_matrix/client/versions');

        $headers = $client->getResponse()->headers;

        $expected = [
            '*',
            'GET, POST, PUT, DELETE, OPTIONS',
            'X-Requested-With, Content-Type, Authorization',
        ];

        $this->assertEquals($expected, [
            $headers->get('Access-Control-Allow-Origin'),
            $headers->get('Access-Control-Allow-Methods'),
            $headers->get('Access-Control-Allow-Headers'),
        ]);

        // On OPTIONS requests we should get CORS headers but no content
        $this->assertEmpty($client->getResponse()->getContent());
    }
}
