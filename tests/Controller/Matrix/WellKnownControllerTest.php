<?php

namespace App\Tests\Controller\Matrix;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WellKnownControllerTest extends WebTestCase
{
    public function testDefault(): void
    {
        $client = static::createClient();

        $client->xmlHttpRequest('GET', '/.well-known/matrix/client');
        $content = json_decode($client->getResponse()->getContent(), true);

        // Check value in `.env.test`
        $this->assertEquals('http://myserver.wip', $content['m.homeserver']['base_url']);
    }
}
