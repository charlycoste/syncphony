{ pkgs ? import <nixpkgs> {}}:

let

php = pkgs.php.buildEnv {
  extraConfig = "memory_limit=-1";
};

in

  pkgs.stdenv.mkDerivation {
    name = "project";
    src = ./.;

    buildPhase = ''
      SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt
      ${php.packages.composer}/bin/composer install --prefer-dist --optimize-autoloader
      ${php}/bin/php bin/console cache:clear --no-warmup
    '';

    installPhase = ''
      mkdir $out
      cp -r composer.json composer.lock bin config public src vendor $out
    '';
  }
