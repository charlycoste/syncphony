{ pkgs ? import <nixpkgs> {}}:

let

php = pkgs.php.buildEnv {
  extraConfig = "memory_limit=-1";
};

in

pkgs.mkShell {
  name = "dev";

  packages = [
    php
    php.packages.composer
    php.packages.php-cs-fixer
    php.packages.psalm
    pkgs.symfony-cli
    pkgs.sqlitebrowser
  ];
}