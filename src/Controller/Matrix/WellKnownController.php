<?php

namespace App\Controller\Matrix;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Routing\Annotation\Route;

#[Route(
    path: '.well-known/matrix/client',
    name: 'app_well_known_client',
    methods: ['GET']
)]
class WellKnownController extends AbstractController
{
    public function __invoke(
        #[Autowire(param: 'homeserver')] string $homeserver
    ): array {
        return [
            'm.homeserver' => [
                'base_url' => $homeserver,
            ],
        ];
    }
}
