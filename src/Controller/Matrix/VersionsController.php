<?php

namespace App\Controller\Matrix;

use Symfony\Component\Routing\Annotation\Route;

#[Route(
    path: '_matrix/client/versions',
    name: 'app_versions',
    methods: ['GET']
)]
class VersionsController
{
    public function __invoke(): array
    {
        return [
            'versions' => [
                'v1.8',
            ],
        ];
    }
}
